
/* Niel Van Schelvergem
   nielvs2011@hotmail.com
   1e jaar Multec aan de Erasmushogeschool te Brussel
   2017-2018
*/




//Coördinates of cube
float middleX, middleY, top, bottom, left, right, topCorner, bottomCorner;

//Variables for myAnimation's
int size = 10;
boolean playMyAnimation1 = true;
boolean playMyAnimation2= false;
boolean playMyAnimation3= false;
boolean playMyAnimation4= false;
//Array met waarden kleur
int[] kleur;




void setup() {
  kleur=new int[3];
  kleur[0]=frameCount; //kleur[0]= frameCount wordt in elke functie herhaalt omdat deze altijd veranderd wanneer deze alleen in setup staat zal de kleur van de eerste framecount behouden worden
  kleur[1]=360;        
  kleur[2]=100;



  size(1280, 720); 

  colorMode(HSB, 360, 100, 100, 100);
  setGlobalVariables();
  initializeMinim();
}



void draw() {
  //White Cube planes
  background(0, 0, 100);
  stroke(0);
  // strokeWeight kan veranderd worden door op de pijltjes up of down te toetsen, om de normale dikte te krijgen druk je op eenderwelke toets
  strokeWeight(5);
  if (keyCode == UP) {
    strokeWeight(10);
  }
  if (keyCode == DOWN) {
    strokeWeight(1);
  }
  //checks each frame if there is a beat.
  beat.detect(player.mix);

  drawAnimations();
  drawCubeMask();
}

void keyReleased() {
  //toggle Animations ON or OFF
  if (key == 'u') {
    playMyAnimation1 = !playMyAnimation1;
  }
  if (key == 'i') {
    playMyAnimation2 = !playMyAnimation2;
  }

  if (key == 'o') {
    playMyAnimation3 = !playMyAnimation3;
  }

  if (key == 'p') {
    playMyAnimation4 = !playMyAnimation4;
  }
}





void drawAnimations() {
  //play animation if toggled ON
  if (playMyAnimation1 == true) {
    myAnimation1();
  }
  if (playMyAnimation2 == true) {
    myAnimation2();
  }
  if (playMyAnimation3 == true) {
    myAnimation3();
  }
  if (playMyAnimation4 == true) {
    myAnimation4();
  }
}
// 
void myAnimation1() {

  if (beat.isOnset()) {
    size = 50;
  }

  kleur[0]=frameCount; // herhaling
  fill( kleur[0] % kleur[1], kleur[2], kleur[2]);

  for ( int i=0; i<30; i++) {
    for ( int j=0; j<30; j++) {
      cube(middleX-640+i*100, middleY-250+j*100, size-100, size-100);
    }
  }


  size +=20;
}
void cube(float x, float y, float cubeWidth, float cubeHeight)
{
  float x1 = x-cubeWidth/2;
  float x2 = x+cubeWidth/2;
  float y1 = y-cubeHeight- cubeWidth/4;
  float y2 = y-cubeHeight;
  float y3 = y-cubeHeight+ cubeWidth/4;
  float y4 = y+ cubeWidth/4;

  quad(x1, y, x1, y2, x, y3, x, y4);

  quad(x2, y, x2, y2, x, y3, x, y4);

  quad(x, y1, x2, y2, x, y3, x1, y2);
}




void myAnimation2() {
  if (beat.isOnset()) {
    size = 10;
  }

  kleur[0]=frameCount; // herhaling
  fill( kleur[0] % kleur[1], kleur[2], kleur[2]);

  ellipse(middleX, middleY, size-100, size+600);
  size +=20;
}


void myAnimation3() {
  if (beat.isOnset()) {
    size = 100;
  }
  tunnel();
  size +=5;
}

void tunnel() {
  strokeWeight(1);
  kleur[0]=frameCount; // herhaling
  stroke( kleur[0] % kleur[1], kleur[2], kleur[2]);

  float y0= height/2;
  float x0= width/2;
  float a0= size / 10;
  float r= 1280;
  int n= 100;

  for (int i= 0; i < n; i++) {

    float a1= a0+TWO_PI*i/n;
    float x= x0+cos(a1)*r;
    float y= y0+sin(a1)*r;

    line(x0, y0, x, y);
  }
}

// Reageert niet op de beat maar kan voor een mooi effect zorgen.
void myAnimation4() {
  if (beat.isOnset()) {
    size =100 ;
  }
  kleur[0]=frameCount; // herhaling
  fill( kleur[0] % kleur[1], kleur[2], kleur[2]);
  cirkels();
}

void cirkels() { 
  float y0= height/2;
  float x0= width/2;
  float a0=(frameCount*0.05)% TWO_PI;
  ;
  float r= 300;
  int n= 20;
  noStroke();
  for (int i= 0; i < n; i++) {

    float a1= a0+TWO_PI*i/n;
    float x= x0+sin(a1)*r;
    float y= y0+cos(a1)*r;

    ellipse(x, y, 100, 100);
  }
  size +=5;
}